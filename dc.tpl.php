<?php
/**
 * @file
 * Default theme file for dc visualizations.
 */
?>
<div <?php print $attributes; ?> class="<?php print implode(' ', $classes_array); ?>"></div>
