<?php
/**
 * @file
 * Views style plugins to render nodes in the XML data format.
 *
 * @see views_plugin_style_xml.inc
 * @ingroup views_plugins
 */

/**
 * Implements hook_views_plugins().
 */
function d3_views_plugins() {

  $path = drupal_get_path('module', 'dc_views') . '/views';
  return array(
    'module' => 'dc_views',
    'style' => array(
      'dc_style' => array(
        'title'           => t('DC Visualization'),
        'path'            => $path . '/plugins',
        'help'            => t('Display view as a DC visualization'),
        'handler'         => 'dc_views_plugin_style_dc',
        'theme'           => 'dc_views_view_dc',
        'theme path'      => $path . '/theme',
        'theme file'      => 'dc_views_view_dc.theme.inc',
        'uses row plugin' => FALSE,
        'uses fields'     => TRUE,
        'uses options'    => TRUE,
        'type'            => 'normal',
        'help_topic'      => 'style-xml',
        'even empty'      => TRUE,
      ),
    ),
  );
}
